module.exports = {
    root: true,
    ignorePatterns: ["dist/**/*"],
    overrides: [
        {
            files: [
                "*.ts"
            ],
            parser: "@typescript-eslint/parser",
            extends: [
                "eslint:recommended",
                "plugin:@typescript-eslint/eslint-recommended",
                "plugin:@typescript-eslint/recommended"
            ],
            plugins: [],
            rules: {
                "@typescript-eslint/array-type": [
                    "error",
                    {
                        "default": "array"
                    }
                ],
                "@typescript-eslint/consistent-type-definitions": "error",
                "@typescript-eslint/dot-notation": "off",
                "@typescript-eslint/explicit-member-accessibility": [
                    "off",
                    {
                        "accessibility": "explicit"
                    }
                ],
                "@typescript-eslint/member-delimiter-style": [
                    "error",
                    {
                        "multiline": {
                            "delimiter": "semi",
                            "requireLast": true
                        },
                        "singleline": {
                            "delimiter": "comma",
                            "requireLast": false
                        }
                    }
                ],
                "@typescript-eslint/member-ordering": "off",
                "@typescript-eslint/naming-convention": [
                    "error",
                    {
                        "selector": "variable",
                        "format": [
                            "camelCase",
                            "UPPER_CASE",
                            "snake_case",
                            "PascalCase"
                        ]
                    },
                    {
                        "selector": ['variable', 'function', 'memberLike'],
                        "modifiers": ["protected"],
                        "format": ["camelCase"],
                        "leadingUnderscore": "require"
                    },
                    {
                        "selector": ['variable', 'function', 'memberLike'],
                        "modifiers": ["private"],
                        "format": ["camelCase"],
                        "leadingUnderscore": "require"
                    }
                ],
                "@typescript-eslint/no-empty-interface": "off",
                "@typescript-eslint/no-namespace": [
                    "off",
                    {
                        "allowDeclarations": true,
                        "allowDefinitionFiles": true
                    }
                ],
                "@typescript-eslint/no-inferrable-types": [
                    "off",
                    {
                        "ignoreParameters": true
                    }
                ],
                "@typescript-eslint/quotes": [
                    "error",
                    "double",
                    {
                        "allowTemplateLiterals": true
                    }
                ],
                "@typescript-eslint/semi": [
                    "error"
                ],
                "@typescript-eslint/no-unused-expressions": [
                    "error"
                ],
                "@typescript-eslint/explicit-function-return-type": [
                    "error",
                    {
                        "allowExpressions": true,
                    }
                ],
                "arrow-body-style": [
                    "error",
                    "as-needed"
                ],
                "brace-style": [
                    "error",
                    "1tbs"
                ],
                "comma-dangle": [
                    "error",
                    {
                        "arrays": "always-multiline",
                        "objects": "always-multiline",
                        "imports": "always-multiline",
                        "exports": "always-multiline",
                        "functions": "always-multiline"
                    }
                ],
                "complexity": [
                    "error",
                    {
                        "max": 200
                    }
                ],
                "import/no-deprecated": "off",
                "jsdoc/no-types": "off",
                "max-len": "off",
                "no-console": [
                    "error",
                    {
                        "allow": [
                            "null"
                        ]
                    }
                ],
                "no-redeclare": "off",
                "no-undef-init": "off",
                "no-underscore-dangle": "off",
                "no-mixed-spaces-and-tabs": [
                    "error"
                ],
                "no-restricted-imports": [
                    "error",
                    {
                        "paths": [],
                        "patterns": [
                            "rxjs/Rx",
                            "@app/generated-api",
                            "**/generated-api"
                        ]
                    }
                ],
                "object-shorthand": "off"
            }
        },
    ]
}
