# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.2 (2022-03-01)


### Features

* init with copyright header rule ([a586c62](https://gitlab.unige.ch/solidify/solidify-eslint-plugin/commit/a586c625cacf41a7569a847d9950e8e8d55682dc))


### Bug Fixes

* add in ignore pattern dist folder ([d66e3d9](https://gitlab.unige.ch/solidify/solidify-eslint-plugin/commit/d66e3d9a11744dcff2886d1dfb49bab4ef470b52))
* lint error ([4ecca31](https://gitlab.unige.ch/solidify/solidify-eslint-plugin/commit/4ecca31bea218bc861b25d2bfb7b550177a4bec8))

### 0.0.2 (2022-03-01)


### Features

* init with copyright header rule ([a586c62](https://gitlab.unige.ch/solidify/solidify-eslint-plugin/commit/a586c625cacf41a7569a847d9950e8e8d55682dc))


### Bug Fixes

* add in ignore pattern dist folder ([d66e3d9](https://gitlab.unige.ch/solidify/solidify-eslint-plugin/commit/d66e3d9a11744dcff2886d1dfb49bab4ef470b52))
* lint error ([4ecca31](https://gitlab.unige.ch/solidify/solidify-eslint-plugin/commit/4ecca31bea218bc861b25d2bfb7b550177a4bec8))
