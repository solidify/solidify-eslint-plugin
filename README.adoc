= Solidify-ESLint-Plugin

image::eslint.png[DLCM Technology,200]

ESLint plugin for Solidify lib and app that use Solidify lib.

== Development

If you want to try your development without releasing the plugin, you can process like below :

- Into the package.json of the consumer project, add or replace `eslint-plugin-solidify` with a relative path instead of version number

----
"devDependencies": {
    ....
    "eslint-plugin-solidify": "../solidify-eslint-plugin/dist",
    ....
}
----

- after each change that you want to test, run inside this folder the command below to generate binary

----
npm run build
----

You can now test your rule freshly edited with `ng lint` command into the consumer project.

NB: If you want to see the rule updated in you IDE, you need probably to restart your IDE.
But for JetBrains users (IntelliJ / WebStorm), you can use the plugin https://plugins.jetbrains.com/plugin/14119-eslint-restart-service-action[ESLint Restart Service Action] to allow to restart ESLint Service and see you development without restarting your IDE.
Once plugin is installed, you find the action to restart into "Help" > "Find Action..." > "Restart ESLint Service"

== Usage

Reminder: To use this eslint plugin in a npm project you need to start by install it with

----
npm i eslint-plugin-solidify
----

Then in your `.eslintrc.js` add `solidify` to the plugin section.

----
...
plugins: [
    "solidify",
],
...
----

== Rules

Reminder : To enable a rule, add the expected rule in `rules` section in your `.eslintrc.js`

----
rules: {
    "solidify/copyright-header": ["error",
        {
            "file": "copyright-header.js",
            "inceptionYear": "2017",
        }
    ],
    ....
}
----

=== Copyright header

Ensure that typescript file begin with copyright comment.
This rule check that the first comment in every file has the contents defined in the rule settings.

==== Usage

----
rules: {
    "solidify/copyright-header": ["error",
        {
            "file": "copyright-header.js",
            "inceptionYear": "2017",
        }
    ],
    ....
}
----

- file : the path to the file that describe the copyright comment expected on top of each typescript files.
You can inject in this template parameters like `{{inceptionYear}}`, `{{currentYear}}` and `{{fileName}}`.
- inceptionYear: the start date of the project.
Necessary if you inject `{{inceptionYear}}` in you template.

Reminder: If you yan to apply all header comment in all files, you can use the command :

----
ng lint --fix
----

== Contributing to project

Please read our link:CONTRIBUTING.adoc[contributing guidelines] before contributing to the project.
