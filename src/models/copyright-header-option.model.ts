export interface CopyrightHeaderOption {
  file?: string;
  commentType?: "block" | "line";
  comment?: string | string[];
  pattern?: string;
  template?: string;
  inceptionYear?: string;
}
