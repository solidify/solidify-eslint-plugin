"use strict";

import {RuleContext} from "@typescript-eslint/utils/dist/ts-eslint";
import {Rule} from "eslint";
import {CopyrightHeaderOption} from "../models/copyright-header-option.model";
import {createEslintRule} from "../utils/create-eslint-rule";

export const RULE_NAME = "copyright-header";
export type MessageIds = "copyrightHeaderRequired";
export type Options = [];

// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require("fs");

export default createEslintRule<Options, MessageIds>({
  name: RULE_NAME,
  meta: {
    type: "layout",
    docs: {
      description: "force file to start with copyright information",
      recommended: "error",
    },
    fixable: "whitespace",
    schema: [
      {
        type: "object",
        properties: {
          "file": {
            type: "string",
          },
          "inceptionYear": {
            type: "string",
          },
        },
        additionalProperties: false,
      },
    ],
    messages: {
      copyrightHeaderRequired: "The file should start with copyright information",
    },
  },
  defaultOptions: [],
  create: (context: Readonly<RuleContext<MessageIds, Options>>) => copyrightHeaderRuleFunc(context as any) as any,
});

const isPattern: (object: any) => boolean = object => typeof object === "object" && Object.prototype.hasOwnProperty.call(object, "pattern");

const match: (actual: any, expected: any) => boolean = (actual, expected) => {
  if (expected.test) {
    return expected.test(actual);
  } else {
    return expected === actual;
  }
};

const excludeShebangs: (comments: any) => any = comments => comments.filter((comment: any) => comment.type !== "Shebang");

// Returns either the first block comment or the first set of line comments that
// are ONLY separated by a single newline. Note that this does not actually
// check if they are at the start of the file since that is already checked by
// hasHeader().
const getLeadingComments: (context: any, node: any) => any = (context, node) => {
  let i;
  const all = excludeShebangs(context.getSourceCode().getAllComments(node.body.length ? node.body[0] : node));
  if (all[0].type.toLowerCase() === "block") {
    return [all[0]];
  }
  for (i = 1; i < all.length; ++i) {
    const txt = context.getSourceCode().getText().slice(all[i - 1].range[1], all[i].range[0]);
    if (!txt.match(/^(\r\n|\r|\n)$/)) {
      break;
    }
  }
  return all.slice(0, i);
};

function genCommentBody(commentType: any, textArray: any, eol: any, numNewlines: any): string {
  const eols = eol.repeat(numNewlines);
  if (commentType === "block") {
    return "/*" + textArray.join(eol) + "*/" + eols;
  } else {
    return "//" + textArray.join(eol + "//") + eols;
  }
}

const genCommentsRange: (context: any, comments: any, eol: any) => any[] = (context, comments, eol) => {
  const start = comments[0].range[0];
  let end = comments.slice(-1)[0].range[1];
  if (context.getSourceCode().text[end] === eol) {
    end += eol.length;
  }
  return [start, end];
};

function genPrependFixer(commentType: any, node: any, headerLines: any, eol: any, numNewlines: any) {
  return function(fixer: any) {
    return fixer.insertTextBefore(
      node,
      genCommentBody(commentType, headerLines, eol, numNewlines),
    );
  };
}

function genReplaceFixer(commentType: any, context: any, leadingComments: any, headerLines: any, eol: any, numNewlines: any) {
  return function(fixer: any) {
    return fixer.replaceTextRange(
      genCommentsRange(context, leadingComments, eol),
      genCommentBody(commentType, headerLines, eol, numNewlines),
    );
  };
}

const hasHeader: (src: any) => boolean = src => {
  if (src.substr(0, 2) === "#!") {
    const m = src.match(/(\r\n|\r|\n)/);
    if (m) {
      src = src.slice(m.index + m[0].length);
    }
  }
  return src.substr(0, 2) === "/*" || src.substr(0, 2) === "//";
};

const matchesLineEndings: (src: any, num: any) => boolean = (src, num) => {
  for (let j = 0; j < num; ++j) {
    const m = src.match(/^(\r\n|\r|\n)/);
    if (m) {
      src = src.slice(m.index + m[0].length);
    } else {
      return false;
    }
  }
  return true;
};

export function fileCopyrightParser(text: string, option: CopyrightHeaderOption): void {
  text = text.trim();
  if (text.substring(0, 2) === "//") {
    option.comment = text.split(/\r?\n/).map((line) => line.substring(2));
    option.commentType = "line";
    return;
  } else if (text.substring(0, 2) === "/*" && text.substring(text.length - 2) === "*/") {
    option.comment = text.substring(2, text.length - 2);
    option.commentType = "block";
    return;
  } else {
    throw new Error("Could not parse copyright file: the file must contain either just line comments (//) or a single block comment (/* ... */)");
  }
}

export function injectParameters(context: Rule.RuleContext, option: CopyrightHeaderOption): void {
  let fileName = context.getFilename();
  fileName = replaceAll(fileName, "\\", "/");
  fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
  const currentYear = String(new Date().getUTCFullYear());
  option.comment = (option.comment as string).replace("{{fileName}}", fileName);
  option.comment = (option.comment as string).replace("{{inceptionYear}}", option.inceptionYear ? option.inceptionYear : currentYear);
  option.comment = (option.comment as string).replace("{{currentYear}}", currentYear);
}

export function replaceAll(str: string, oldChar: string, newChar: string): string {
  const strSplitter = str.split(oldChar);
  return strSplitter.join(newChar);
}

export function copyrightHeaderRuleFunc(context: Rule.RuleContext): Rule.RuleListener {
  const option = context.options[0] as CopyrightHeaderOption;
  const numNewlines = 2;
  const eol = "\n";

  if (option.file) {
    const text = fs.readFileSync(option.file, "utf8");
    fileCopyrightParser(text, option);
  }

  injectParameters(context, option);

  const commentType = option.commentType.toLowerCase();
  let headerLines: any, fixLines: any = [];
  // If any of the lines are regular expressions, then we can't
  // automatically fix them. We set this to true below once we
  // ensure none of the lines are of type RegExp
  let canFix = false;
  if (Array.isArray(option.comment)) {
    throw new Error("A");
    // canFix = true;
    // headerLines = option.comment.map((line) => {
    //   const isRegex = isPattern(line);
    //   // Can only fix regex option if a template is also provided
    //   if (isRegex && !line.template) {
    //     canFix = false;
    //   }
    //   fixLines.push(line.template || line);
    //   return isRegex ? new RegExp(line.pattern) : line;
    // });
  } else if (isPattern(option.comment)) {
    throw new Error("B");

    // const line = option.comment;
    // headerLines = [new RegExp(line.pattern)];
    // fixLines.push(line.template || line);
    // // Same as above for regex and template
    // canFix = !!line.template;
  } else {
    canFix = true;
    headerLines = option.comment.split(/\r?\n/);
    fixLines = headerLines;
  }

  // fileName
  // inceptionYear
  // currentYear

  return {
    Program: function(node) {
      if (!hasHeader(context.getSourceCode().getText())) {
        context.report({
          loc: node.loc,
          message: "missing header",
          fix: genPrependFixer(commentType, node, fixLines, eol, numNewlines),
        });
      } else {
        const leadingComments = getLeadingComments(context, node);

        if (!leadingComments.length) {
          context.report({
            loc: node.loc,
            message: "missing copyright header",
            fix: canFix ? genPrependFixer(commentType, node, fixLines, eol, numNewlines) : null,
          });
        } else if (leadingComments[0].type.toLowerCase() !== commentType) {
          context.report({
            loc: node.loc,
            message: "copyright header should be a {{commentType}} comment",
            data: {
              commentType: commentType,
            },
            fix: canFix ? genReplaceFixer(commentType, context, leadingComments, fixLines, eol, numNewlines) : null,
          });
        } else {
          if (commentType === "line") {
            if (leadingComments.length < headerLines.length) {
              context.report({
                loc: node.loc,
                message: "incorrect copyright header",
                fix: canFix ? genReplaceFixer(commentType, context, leadingComments, fixLines, eol, numNewlines) : null,
              });
              return;
            }
            for (let i = 0; i < headerLines.length; i++) {
              if (!match(leadingComments[i].value, headerLines[i])) {
                context.report({
                  loc: node.loc,
                  message: "incorrect copyright header",
                  fix: canFix ? genReplaceFixer(commentType, context, leadingComments, fixLines, eol, numNewlines) : null,
                });
                return;
              }
            }

            const postLineHeader = context.getSourceCode().text.substr(leadingComments[headerLines.length - 1].range[1], numNewlines * 2);
            if (!matchesLineEndings(postLineHeader, numNewlines)) {
              context.report({
                loc: node.loc,
                message: "no newline after copyright header",
                fix: canFix ? genReplaceFixer(commentType, context, leadingComments, fixLines, eol, numNewlines) : null,
              });
            }

          } else {
            // if block comment pattern has more than 1 line, we also split the comment
            let leadingLines = [leadingComments[0].value];
            if (headerLines.length > 1) {
              leadingLines = leadingComments[0].value.split(/\r?\n/);
            }

            let hasError = false;
            if (leadingLines.length > headerLines.length) {
              hasError = true;
            }
            for (let i = 0; !hasError && i < headerLines.length; i++) {
              if (!match(leadingLines[i], headerLines[i])) {
                hasError = true;
              }
            }

            if (hasError) {
              if (canFix && headerLines.length > 1) {
                fixLines = [fixLines.join(eol)];
              }
              context.report({
                loc: node.loc,
                message: "incorrect copyright header",
                fix: canFix ? genReplaceFixer(commentType, context, leadingComments, fixLines, eol, numNewlines) : null,
              });
            } else {
              const postBlockHeader = context.getSourceCode().text.substr(leadingComments[0].range[1], numNewlines * 2);
              if (!matchesLineEndings(postBlockHeader, numNewlines)) {
                context.report({
                  loc: node.loc,
                  message: "no newline after copyright header",
                  fix: canFix ? genReplaceFixer(commentType, context, leadingComments, fixLines, eol, numNewlines) : null,
                });
              }
            }
          }
        }
      }
    },
  } as Rule.RuleListener;
}
