import copyrightHeaderRule from "./copyright-header-rule";
import readonlyInjectablesRule from "./readonly-injectables.rule";

export default {
  "copyright-header": copyrightHeaderRule,
  "readonly-injectables": readonlyInjectablesRule,
};
